import time

import pytest
from selenium.common import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import calendar
from datetime import datetime
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.common.by import By

chrome_options = ChromeOptions()
chrome_options.add_argument("--disable-application-cache")

driver = webdriver.Chrome(
    service=ChromeService(ChromeDriverManager().install()), options=chrome_options)
wait = WebDriverWait(driver, 10)


def set_date():
    driver.find_element(By.XPATH,
                        '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[1]/div[2]/div/div').click()
    driver.find_element(By.XPATH, "//button[@aria-label='Previous Year']").click()
    driver.find_element(By.XPATH, "//div[@class='react-datepicker__month-wrapper']//div[text()='февраль']").click()


def navigate_to_documents_tab():
    # Click through the necessary elements to navigate to the "Documents" tab
    try:
        accounting_accordion = wait.until(
            EC.element_to_be_clickable(
                (By.XPATH, '/html/body/div[1]/div/section/section/div/div[1]/div/div[2]/div[5]/div[3]/div/div[1]/div[2]')))
        accounting_accordion.click()
        print("Бухгалтерия")
        reports_and_documents = wait.until(
            EC.element_to_be_clickable((By.XPATH, "//div[contains(text(),'Отчеты и документы')]")))
        reports_and_documents.click()
        print("Отчеты и Документы")
        documents_tab = wait.until(EC.element_to_be_clickable(
            (By.XPATH, '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[1]/button[2]')))
        documents_tab.click()
        print("Navigated to Documents tab")
    except NoSuchElementException:
        print("Failed to navigate to Documents tab")


@pytest.fixture(scope="module")
def setup_teardown():
    driver.maximize_window()
    driver.get("https://dimash-admin.1fit.app/login")
    driver.find_element(By.ID, "login_email").send_keys("assemay@1fit.app")
    driver.find_element(By.ID, "login_password").send_keys("1FIT2021!")
    driver.find_element(By.XPATH, "//button[@type='submit']").click()
    print("Log in")
    navigate_to_documents_tab()
    set_date()
    print("Дата: 01/2022")
    yield
    time.sleep(3)
    print("Finished Test")


@pytest.mark.parametrize("datetime_input, month_text", [
    ("01/2022", "январь"),
    ("03/2022", "март")
])
def test_date_picker(datetime_input, month_text, setup_teardown):
    driver.find_element(By.XPATH,
                        '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[1]/div[2]/div/div').click()
    driver.find_element(By.XPATH,
                        f"//div[@class='react-datepicker__month-wrapper']//div[text()='{month_text}']").click()

    # Dynamically change the input date value based on selected month and year
    month, year = datetime_input.split('/')
    last_day = calendar.monthrange(int(year), int(month))[1]
    chosen_date_formatted = f"{year}-{month}-01 - {year}-{month}-{last_day}"

    print('date chosen')
    rows = wait.until(EC.visibility_of_all_elements_located((By.XPATH, "//tbody//tr")))
    print('test_date_picker, Number of rows:', len(rows))
    for row in rows:
        periods_column = row.find_elements(By.XPATH, "//tbody//tr//td[3]")
        for period in periods_column:
            period_value = period.text
            assert period_value == chosen_date_formatted, f"Period value {period_value} does not match {chosen_date_formatted}"


@pytest.mark.parametrize("option_id, expected_status", [
    # ('react-select-5-option-0', 'Дата '),
    ('react-select-5-option-1', 'Подтвердить получение')

])
def test_fact_received(setup_teardown, option_id, expected_status):
    driver.find_element(By.XPATH, '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div').click()
    option = driver.find_element(By.ID, option_id)
    option.click()
    rows = wait.until(EC.visibility_of_all_elements_located((By.XPATH, "//tbody//tr")))
    print('test_fact_received, Number of rows:', len(rows))
    for row in rows:
        received_statuses_column = row.find_elements(By.XPATH, "//tbody//tr//td[6]")
        print('Number of status elements in column:', len(received_statuses_column))
        for status in received_statuses_column:
            status_value = status.text
            print('status:', status_value)
            assert status_value == expected_status, f"Status value {status_value} Not '{expected_status}' "
    cross_button = driver.find_element(By.XPATH,
                                               '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div/div[2]/div[1]')
    cross_button.click()


@pytest.mark.parametrize("option_id, expected_status", [
    # ('react-select-4-option-0', 'Дата'),
    ('react-select-4-option-1', 'Нет')
])
def test_fact_sent(setup_teardown, option_id, expected_status):
    driver.find_element(By.XPATH, '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[1]/div[3]/div/div').click()
    option = driver.find_element(By.ID, option_id)
    option.click()
    rows = wait.until(EC.visibility_of_all_elements_located((By.XPATH, "//tbody//tr")))
    print('test_fact_sent, Number of rows:', len(rows))
    for row in rows:
        sent_statuses_column = row.find_elements(By.XPATH, "//tbody//tr//td[5]")
        print('Number of status elements in column:', len(sent_statuses_column))
        for status in sent_statuses_column:
            status_value = status.text
            print('status:', status_value)
            assert status_value == expected_status, f"Status value {status_value} Not '{expected_status}' "
    cross_button = driver.find_element(By.XPATH, '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[1]/div[3]/div/div/div[2]/div[1]')
    cross_button.click()


@pytest.mark.skip(reason='Blocked! Backend issue')
def test_confirm_received_button(setup_teardown):
    wait.until(EC.element_to_be_clickable((By.XPATH,
                                           '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[3]/div[2]/button[4]'))).click()

    driver.find_element(By.XPATH,
                        '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[1]/div[4]/div/div').click()

    rows = wait.until(EC.visibility_of_all_elements_located((By.XPATH, "//tbody//tr")))
    print('test_fact_received, Number of rows:', len(rows))

    for row in rows:
        received_statuses_column = row.find_elements(By.XPATH, "//tbody//tr//td[6]")
        print('Number of status elements in column:', len(received_statuses_column))
        c = 0
        for status in received_statuses_column:
            status_value = status.text

            if status_value == "Подтвердить получение":
                print("Found: Подтвердить получение")
                status.click()
                confirm = wait.until(EC.element_to_be_clickable((By.XPATH, "//button[text()='Подтвердить']")))
                c += 1
                confirm.click()
                clicked_row = row  # Store the clicked row
                print("clicked row ", clicked_row.text)
                time.sleep(2)
                # Wait for the status to update in the clicked row
                updated_status_element = wait.until(
                    EC.visibility_of_element_located((By.XPATH, ".//td[6]")),
                    message="Status update in clicked row"
                )
                updated_status_value = updated_status_element.text

                print(f"Status changed to: {updated_status_value}")  # Print the updated status text

                # datetime_element = clicked_row.find_element(By.XPATH, ".//td[6]")  # Adjust the column index if needed
                # updated_datetime_value = datetime_element.text

                current_datetime = datetime.now()
                formatted_datetime = current_datetime.strftime('%d.%m.%Y %H:%M')
                print(updated_status_value + " received time and current time " + formatted_datetime)
                assert updated_status_value == formatted_datetime


@pytest.mark.parametrize("document_type_id, expected_type", [
    (0, 'СФ'),
    (1, 'АВР')
])
def test_filter_and_check_document_type(setup_teardown, document_type_id, expected_type):
    filter_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="tabs-:rc:--tabpanel-1"]/div/div[1]/div[5]/div/div')))
    filter_button.click()

    document_option = driver.find_element(By.ID, f'react-select-6-option-{document_type_id}')
    document_option.click()

    rows = wait.until(EC.visibility_of_all_elements_located((By.XPATH, "//tbody//tr")))

    for row in rows:
        document_type_column = row.find_element(By.XPATH, ".//td[4]").text

        assert document_type_column == expected_type, f"Unexpected document type: {document_type_column}"

        if document_type_column == "АВР":
            download_button = driver.find_element(By.XPATH,
                                                  '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[2]/table/tbody/tr[1]/td[7]/div/button')
            assert download_button.is_displayed(), "Download icon not found for 'АВР' type"
        else:
            empty_space = driver.find_element(By.XPATH, '//*[@id="tabs-:rc:--tabpanel-1"]/div/div[2]/table/tbody/tr[1]/td[7]')
            assert empty_space.is_displayed(), "Unexpected download button for 'СФ' type"
    cross_button = driver.find_element(By.XPATH, '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[1]/div[5]/div/div/div[2]/div[1]')
    cross_button.click()


# Test data for test_search
search_data = [
    ("581114401562", "581114401562"),
    ("790530402285", "790530402285"),
]


@pytest.mark.parametrize("search_input, expected_iin", search_data)
def test_search(setup_teardown, search_input, expected_iin):
    search = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[1]/div[1]/input')))
    search.send_keys(search_input)
    rows = wait.until(EC.presence_of_all_elements_located((By.XPATH, "//tbody//tr")))
    print('test_search, Number of rows:', len(rows))

    search.clear()
    for row in rows:
        iin_columns = row.find_elements(By.XPATH, "//tbody//tr//td[2]")

        for iin_column in iin_columns:
            iin_value = iin_column.text

            assert iin_value == expected_iin, f"IIN value {iin_value} does not match {expected_iin}"
    search.clear()
    search.send_keys(" ")


@pytest.mark.parametrize("num, idx", [
    (1, '1'), (2, '2'), (3, '3'), (4, '4')
    , (7, '86'), (6, '85'), (1, '1')

])
def test_pagination(setup_teardown, num, idx):

    page = wait.until(EC.element_to_be_clickable((By.XPATH, f'//*[@id="tabs-:rc:--tabpanel-1"]/div/div[3]/div[2]/div/button[{num}]')))
    page.click()
    page_number = wait.until(EC.visibility_of_element_located((By.XPATH, f'//*[@id="tabs-:rc:--tabpanel-1"]/div/div[3]/div[2]/div/button[{num}][contains(text(), "{idx}")]')))
    assert idx == page_number.text, f'Page number : {idx} is not equal to: {page_number.text}'


@pytest.mark.skip(reason='ID Changes Dynamically Each Time, When Run Driver')
@pytest.mark.parametrize("option_idx, select_idx, limit_number", [
    ("1", "24",  25),
    ("2", "24", 50),
    ("3", "24", 100)
])
def test_set_limit(setup_teardown, option_idx, select_idx, limit_number):

    limit = wait.until(EC.element_to_be_clickable((By.XPATH,
                                '/html/body/div[1]/div/section/section/div/div[2]/div/div/div[2]/div/div[3]/div/div[2]/div/div[3]/div[1]/div[2]/div')))
    limit.click()
    dropUp = wait.until(EC.element_to_be_clickable((By.ID, f"react-select-{select_idx}-option-{option_idx}")))
    dropUp.click()
    rows = wait.until(EC.visibility_of_all_elements_located((By.XPATH, "//tbody//tr")))
    assert len(rows) == limit_number








