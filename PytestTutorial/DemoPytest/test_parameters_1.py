import math

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By

@pytest.mark.parametrize("num1, num2, expected_total",
                         [
                            ("25", "25", "50"),
                            ("10", "10", "30"),
                            ("30", "40", "70")
                         ])
def test_lambda_two_input_fields(num1, num2, expected_total):
    dr = webdriver.Chrome()
    dr.maximize_window()
    dr.get("https://www.lambdatest.com/selenium-playground/simple-form-demo")
    dr.find_element(By.ID, "sum1").send_keys(num1)
    dr.find_element(By.ID, "sum2").send_keys(num2)
    dr.find_element(By.XPATH, "//button[text()='Get Sum']").click()
    res = dr.find_element(By.ID, "addmessage").text
    assert res == expected_total, "Actual value != Expected value"


@pytest.mark.parametrize("base", [1, 2, 3])
@pytest.mark.parametrize("exponent", [4, 5, 6])
def test_raising_base_to_power(base, exponent):
    res = base ** exponent
    assert res == math.pow(base, exponent)
