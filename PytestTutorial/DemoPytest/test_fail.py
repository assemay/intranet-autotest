import pytest


class Test_Math:
    def test_divide_number(self):
        pytest.xfail("Need to Investigate")
        num = 10
        res = num + num
        assert res == num / num

    @pytest.mark.xfail(reason="Result Add Numbers & Not Multiply")
    def test_square_number(self):
        num = 10
        result = num + num
        square_number = num * num
        assert result == square_number

    @pytest.mark.xfail(reason="Result & Assert Are Correct")
    def test_cube_number(self):
        num = 10
        res = num * num * num
        assert res == num * num * num

    # run = True, means: RUN
    # run = False, means: NOTRUN
    @pytest.mark.xfail(run=False)
    def test_number_square(self):
        num = 10
        result = num * num
        square_number = num ** 2
        assert result == square_number

