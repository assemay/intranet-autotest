import pytest


# @pytest.mark.skip(reason="Demo How To Skip A Class")
# @pytest.mark.xfail(reason="Demo How To XFail/XPass A Class")
class Test_Math:
    # Pass
    def test_number_square(self):
        num = 10
        result = num * num
        square_number = num ** 2
        assert result == square_number

    def test_divide_number(self):
        # Fail
        num = 10
        res = num + num
        assert res == num / num

    # Fail
    def test_square_number(self):
        num = 10
        result = num + num
        square_number = num * num
        assert result == square_number

    # Pass
    def test_cube_number(self):
        num = 10
        res = num * num * num
        assert res == num * num * num



