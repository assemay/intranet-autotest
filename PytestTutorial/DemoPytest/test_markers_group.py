import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By

pytestmark = [pytest.mark.regression, pytest.mark.sanity]


@pytest.mark.integration
@pytest.mark.smoke
def test_lambdatest_ajax_form():
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get("https://www.lambdatest.com/selenium-playground/ajax-form-submit-demo")
    driver.find_element(By.ID, "title").send_keys("Assemay")
    driver.find_element(By.ID, "description").send_keys("lambda test")
    driver.find_element(By.ID, "btn-submit").click()
    request = driver.find_element(By.ID, "submit-control").text

    assert request.__contains__("Processing")


def test_e2e():
    print("End to end test")


@pytest.mark.smoke
def test_login():
    print("Log in to app")


@pytest.mark.smoke
def test_logout():
    print("Log out from app")
