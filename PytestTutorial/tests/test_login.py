from pages.login_page import LoginPage
from tests.base_test import BaseTest
from utilities.test_data import TestData


class TestLogin(BaseTest):
    def test_valid_credential(self):
        login_page = LoginPage(self.driver)
        login_page.set_email_address(TestData.email)
        login_page.set_password(TestData.password)
        login_page.click_login_button()
        actual_title = login_page.get_title()
        assert actual_title == "My Account"

    def test_invalid_credential(self):
        login_page = LoginPage(self.driver)
        login_page.log_into_appliation("Invalid Email", "Invalid Password")
        actual_msg = login_page.get_warning_msg()
        assert actual_msg.__contains__("Warning")


